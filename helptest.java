import org.junit.*;

public class helptest {
    @Test
    public void test() {
        help tes = new help();
        int result = tes.add(2, 3); // Provide fixed values for testing
        int result2=tes.subtract(4,6);
        int result3=tes.subtract(9,6);
        int result4=tes.multiply(9,6);
        int result5=tes.div(9,3);
        int result6=tes.div(1,3);
        Assert.assertEquals(5, result);
        Assert.assertEquals(-1, result2);
        Assert.assertEquals(3, result3);
        Assert.assertEquals(54, result4);
        Assert.assertEquals(3, result5);
        Assert.assertEquals(0, result6);
    }
}
